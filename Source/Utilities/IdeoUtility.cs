﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone
{
    public static class IdeoUtility
    {
        public static Ideo GetIdeoOf(Pawn pawn)
        {
            Ideo ideo = pawn.Ideo;
            if(ideo != null)
            {
                return ideo;
            }
            ideo = pawn.Faction.ideos.PrimaryIdeo;
            if(ideo != null)
            {
                return ideo;
            }
            return null;
        }
    }
}
