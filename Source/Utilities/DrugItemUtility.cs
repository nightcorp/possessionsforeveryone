﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone
{
    public static class DrugItemUtility
    {
        private static Dictionary<ChemicalDef, List<ThingDef>> cachedDrugs = new Dictionary<ChemicalDef, List<ThingDef>>();
        private static FloatRange DaysSatisfied = AccessTools.StaticFieldRefAccess<FloatRange>(typeof(StartingPawnUtility), "DaysSatisfied");

        public static ThingDef GetDrugThingFor(Hediff hediff, out int possessionCount)
        {
            possessionCount = 0;
            ChemicalDef chemicalDef = GetChemicalDef(hediff);
            if(chemicalDef == null)
            {
                return null;
            }

            if (!cachedDrugs.ContainsKey(chemicalDef))
            {
                CacheDrugFor(chemicalDef);
            }
            if (!cachedDrugs.TryGetValue(chemicalDef, out List<ThingDef> possibleDrugThings))
            {
                return null;
            }
            ThingDef drug = possibleDrugThings.RandomElementByWeight(t => t.generateCommonality);
            possessionCount = ReasonableCountFor(hediff, drug);
            return drug;
        }

        private static int ReasonableCountFor(Hediff hediff, ThingDef thingDef)
        {

            if (hediff is Hediff_Addiction addiction)
            {
                Need need = addiction.Need;
                return GenMath.RoundRandom(need.def.fallPerDay * DaysSatisfied.RandomInRange / thingDef.GetCompProperties<CompProperties_Drug>().needLevelOffset);
            }
if (hediff is Hediff_ChemicalDependency dependency)
            {
                HediffCompProperties_SeverityPerDay hediffCompProperties_SeverityPerDay = dependency.def.CompProps<HediffCompProperties_SeverityPerDay>();
                float severityPerDay = hediffCompProperties_SeverityPerDay != null ? hediffCompProperties_SeverityPerDay.severityPerDay : 1f;
                return GenMath.RoundRandom(DaysSatisfied.RandomInRange * severityPerDay);
            }
            return 0;
        }

        private static void CacheDrugFor(ChemicalDef chemicalDef)
        {
            List<ThingDef> drugThingDefs = DefDatabase<ThingDef>.AllDefsListForReading.Where((ThingDef def) =>
            {
                CompProperties_Drug comp = def.GetCompProperties<CompProperties_Drug>();
                return comp != null && comp.chemical == chemicalDef;
            })
                    .ToList();
            // this potentiall persists NULL values, which is what we want, otherwise it would constantly try to recache for drugs that have no things that cover the chemical
            cachedDrugs[chemicalDef] = drugThingDefs;
        }

        private static ChemicalDef GetChemicalDef(Hediff hediff)
        {
            if (hediff is Hediff_Addiction addictionHediff)
            {
                return addictionHediff.Chemical;
            }
            if (hediff is Hediff_ChemicalDependency dependencyHediff)
            {
                return dependencyHediff.chemical;
            }
            return null;
        }

    }
}
