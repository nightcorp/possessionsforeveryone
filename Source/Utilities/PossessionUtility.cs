﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace PossessionsForEveryone
{
    public static class PossessionUtility
    {
        private static IntRange BabyFoodCountRange = AccessTools.StaticFieldRefAccess<IntRange>(typeof(StartingPawnUtility), "BabyFoodCountRange");
        private static IntRange HemogenCountRange = AccessTools.StaticFieldRefAccess<IntRange>(typeof(StartingPawnUtility), "HemogenCountRange");

        private static List<ThingDef> randomPossessables = DefDatabase<ThingDef>.AllDefsListForReading
            .Where(t => t.possessionCount > 0
                && !t.HasModExtension<ThingExtension_InvalidForPossessionsFlag>())
            .ToList();

        /// <returns>NULL if no possessions could be calculated, otherwise a list of things the pawn would consider its possessions</returns>
        public static List<ThingDefCount> GeneratePossessionsFor(Pawn pawn)
        {
            if(PfESettings.IsDevModeEnabled)
                Log.Message($"Generating possessions for {pawn.LabelShort}");
            List<ThingDefCount> possessions = new List<ThingDefCount>();
            if(Find.Scenario.AllParts.Any(p => p is ScenPart_NoPossessions))
            {
                return new List<ThingDefCount>();
            }

            // baby food
            if(ModsConfig.BiotechActive && pawn.DevelopmentalStage.Baby())
            {
                AddPossession(ThingDefOf.BabyFood, BabyFoodCountRange.RandomInRange);
            }

            // drugs
            foreach(Hediff hediff in pawn.health.hediffSet.hediffs)
            {
                ThingDef drugThingDef = DrugItemUtility.GetDrugThingFor(hediff, out int possessionCount);
                if (drugThingDef != null)
                {
                    AddPossession(drugThingDef, possessionCount);
                }
            }

            // blood packs 
#if v1_4
            if(ModsConfig.BiotechActive && pawn.genes != null && pawn.genes.HasGene(GeneDefOf.Hemogenic))
#else
            if (ModsConfig.BiotechActive && pawn.genes != null && pawn.genes.HasActiveGene(GeneDefOf.Hemogenic))
#endif
            {
                AddPossession(ThingDefOf.HemogenPack, HemogenCountRange.RandomInRange);
            }

            // backstory
            BackstoryDef backstory = pawn.story.GetBackstory(BackstorySlot.Adulthood);
            if(backstory != null)
            {
#if v1_4
                foreach (BackstoryThingDefCountClass backstoryThingDefCountClass in backstory.possessions)
                {
                    AddPossession(backstoryThingDefCountClass.key, Mathf.Min(backstoryThingDefCountClass.key.stackLimit, backstoryThingDefCountClass.value));
                }
#else
                foreach (PossessionThingDefCountClass backstoryThingDefCountClass in backstory.possessions)
                {
                    AddPossession(backstoryThingDefCountClass.key, Mathf.Min(backstoryThingDefCountClass.key.stackLimit, backstoryThingDefCountClass.value.RandomInRange));
                }
#endif
            }

            // random junk
            if(randomPossessables.Any())
            {
                ThingDef randomPossessable = randomPossessables.RandomElementByWeight(t => t.generateCommonality);
                int count = Mathf.Min(randomPossessable.stackLimit, randomPossessable.possessionCount);
                AddPossession(randomPossessable, count);
            }

            return possessions;

            void AddPossession(ThingDef thingDef, int count)
            {
                if (thingDef.HasModExtension<ThingExtension_InvalidForPossessionsFlag>())
                {
                    if(PfESettings.IsDevModeEnabled)
                        Log.Message($"Prevented {thingDef.defName} from being used as a possession due to a {nameof(ThingExtension_InvalidForPossessionsFlag)}");
                    return;
                }
                possessions.Add(new ThingDefCount(thingDef, count));
            }
        }

        public static void GenerateAndGivePossessionsTo(Pawn pawn, out List<ThingDefCount> generatedPossessions)
        {
            generatedPossessions = GeneratePossessionsFor(pawn);
            if(generatedPossessions == null)
            {
                return;
            }
            foreach(ThingDefCount possession in generatedPossessions)
            {
                Thing possessionThing = possession.Generate(pawn);
#if !v1_4
                if(possessionThing is Book book)
                {
                    book.GenerateBook(null);
                }
#endif
                pawn.inventory.innerContainer.TryAdd(possessionThing);
            }
        }

        public static Thing Generate(this ThingDefCount thingDefCount, Pawn pawn = null)
        {
            ThingDef thingDef = thingDefCount.ThingDef;
            if(PfESettings.IsDevModeEnabled) 
                Log.Message($"Trying to generate {thingDef} x {thingDefCount.Count}");
            Thing thing = ThingMaker.MakeThing(thingDef, GenStuff.RandomStuffFor(thingDef));
            if(thing.def.Minifiable)
            {
                thing = thing.MakeMinified();
            }
            if(thingDef.IsIngestible && thingDef.ingestible.IsMeal)
            {
                Ideo pawnIdeo = IdeoUtility.GetIdeoOf(pawn);
                if(pawnIdeo != null)
                {
                    FoodUtility.GenerateGoodIngredients(thing, pawnIdeo);
                }
            }
            thing.stackCount = thingDefCount.Count;
            return thing;
        }
    }
}
