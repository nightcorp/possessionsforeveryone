﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace PossessionsForEveryone
{
    public class PfEMod : Mod
    {
        public static PfESettings PfESettings;

        public PfEMod(ModContentPack content) : base(content)
        {
            PfESettings = GetSettings<PfESettings>();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            PfESettings.DrawSettings(inRect);
        }

        public override string SettingsCategory() => "PfE_Settings_Category".Translate();
    }

    public class PfESettings : ModSettings
    {
        private static bool isDevModeEnabled = false;
        private static bool isPerformanceWatchdogEnabled = false;
        private static bool generateForEveryone = true;
        private static float chanceForEveryone = 0.5f;
        private static float chanceForColonists = 0.5f;
        private static float chanceForRaiders = 0.5f;
        private static float chanceForVisitors = 0.5f;

        public static bool IsDevModeEnabled => Prefs.DevMode && isDevModeEnabled;
        public static bool IsPerformanceWatchdogEnabled => IsDevModeEnabled && isPerformanceWatchdogEnabled;
        public static float ChanceForColonists => generateForEveryone ? chanceForEveryone : chanceForColonists;
        public static float ChanceForRaiders => generateForEveryone ? chanceForEveryone : chanceForRaiders;
        public static float ChanceForVisitors => generateForEveryone ? chanceForEveryone : chanceForVisitors;

        public void DrawSettings(Rect rect)
        {
            Listing_Standard list = new Listing_Standard();
            list.Begin(rect);
            list.Label("PfE_Settings_Info".Translate());
            list.GapLine();
            if(Prefs.DevMode)
            {
                list.CheckboxLabeled("PfE_Settings_DevMode".Translate(), ref isDevModeEnabled, "PfE_Settings_DevMode_Tip".Translate());
                if(IsDevModeEnabled)
                {
                    list.CheckboxLabeled("PfE_Settings_PerformanceWatchdog".Translate(), ref isPerformanceWatchdogEnabled, "PfE_Settings_PerformanceWatchdog_Tip".Translate());
                }
            }
            list.CheckboxLabeled("PfE_Settings_GenerateForEveryone".Translate(), ref generateForEveryone);
            if(generateForEveryone)
            {
                list.Label("PfE_Settings_ChanceForEveryone".Translate(chanceForEveryone.ToStringByStyle(ToStringStyle.PercentZero)));
                chanceForEveryone = list.Slider(chanceForEveryone, 0, 1);
            }
            else
            {
                list.Label("PfE_Settings_ChanceForColonists".Translate(chanceForColonists.ToStringByStyle(ToStringStyle.PercentZero)));
                chanceForColonists = list.Slider(chanceForColonists, 0, 1);
                list.Label("PfE_Settings_ChanceForRaiders".Translate(chanceForRaiders.ToStringByStyle(ToStringStyle.PercentZero)));
                chanceForRaiders = list.Slider(chanceForRaiders, 0, 1);
                list.Label("PfE_Settings_ChanceForVisitors".Translate(chanceForVisitors.ToStringByStyle(ToStringStyle.PercentZero)));
                chanceForVisitors = list.Slider(chanceForVisitors, 0, 1);
            }

            if(IsPerformanceWatchdogEnabled)
            {
                list.GapLine();
                list.Label("PfE_Settings_PerformanceWatchdog_Instructions".Translate());
            }

            list.End();
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref isDevModeEnabled, nameof(isDevModeEnabled));
            Scribe_Values.Look(ref isPerformanceWatchdogEnabled, nameof(isPerformanceWatchdogEnabled));
            Scribe_Values.Look(ref generateForEveryone, nameof(generateForEveryone));
            Scribe_Values.Look(ref chanceForEveryone, nameof(chanceForEveryone));
            Scribe_Values.Look(ref chanceForColonists, nameof(chanceForColonists));
            Scribe_Values.Look(ref chanceForRaiders, nameof(chanceForRaiders));
            Scribe_Values.Look(ref chanceForVisitors, nameof(chanceForVisitors));
        }
    }
}
