﻿#if !v1_4
using LudeonTK;
#endif
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone
{
    [HarmonyPatch(typeof(PawnGenerator), "GenerateOrRedressPawnInternal")]
    public static class PerformanceWatchdog
    {
        private static Stopwatch stopWatch = new Stopwatch();
        private static List<long> elapsedTimes = new List<long>();

        [HarmonyPrefix]
        public static void Start()
        {
            if(!PfESettings.IsPerformanceWatchdogEnabled)
            {
                return;
            }
            stopWatch.Start();
        }

        [HarmonyPostfix]
        public static void End()
        {
            if(!PfESettings.IsPerformanceWatchdogEnabled)
            {
                return;
            }
            long elapsedTime = stopWatch.ElapsedMilliseconds;
            stopWatch.Stop();
            stopWatch.Reset();
            elapsedTimes.Add(elapsedTime);
        }


        [DebugAction("PossessionsForEveryone", "Reset Performance Samples", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void Reset()
        {
            if(!PfESettings.IsPerformanceWatchdogEnabled)
            {
                Log.Message($"Performance watchdog is not enabled in mod settings.");
                return;
            }
            if(stopWatch.IsRunning)
            {
                stopWatch.Stop();
                stopWatch.Reset();
            }
            elapsedTimes.Clear();
        }


        [DebugAction("PossessionsForEveryone", "Log Performance", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void LogPerformance()
        {
            if(!PfESettings.IsPerformanceWatchdogEnabled)
            {
                Log.Message($"Performance watchdog is not enabled in mod settings.");
                return;
            }
            int removedEntries = elapsedTimes.RemoveAll(t => t == 0);
            Log.Message($"Removed {removedEntries} entries that were 0ms");
            if(elapsedTimes.NullOrEmpty())
            {
                Log.Message($"No sample size");
                return;
            }
            string message = $"Sample size: {elapsedTimes.Count}: Average {elapsedTimes.Average()}, Min: {elapsedTimes.Min()}, Max: {elapsedTimes.Max()}";
            Log.Message(message);
        }
    }
}
