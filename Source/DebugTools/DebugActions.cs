﻿#if !v1_4
using LudeonTK;
#endif
using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone.DebugTools
{
    public static class DebugActions
    {
        [DebugAction("PossessionsForEveryone", "Log Possessions For", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void LogPossessionsFor(Pawn p)
        {
            List<ThingDefCount> possessions = PossessionUtility.GeneratePossessionsFor(p);
            Log.Message($"Possessions for {p.LabelShort}: {string.Join(", ", possessions.Select(pos => $"{pos.ThingDef} - {pos.Count}"))}");
        }

        [DebugAction("PossessionsForEveryone", "Log Starting possessions", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void LogStartingPossessions()
        {
            Dictionary<Pawn, List<ThingDefCount>> startingPossessions = Find.GameInitData?.startingPossessions;
            if(startingPossessions == null)
            {
                Log.Message($"NULL starting possessions");
                return;
            }
            string text = $"Starting possessions:";
            foreach(KeyValuePair<Pawn, List<ThingDefCount>> possession in startingPossessions)
            {
                text += $"\n{possession.Key.LabelShort}: {string.Join(", ", possession.Value.Select(tdc => $"{tdc.ThingDef} - {tdc.Count}"))}";
            }
            Log.Message(text);
        }

        
    }
}
