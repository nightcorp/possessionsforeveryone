﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone
{
    [HarmonyPatch(typeof(StartingPawnUtility), "GeneratePossessions")]
    public static class Patch_StartingPawnUtility
    {
        /// <summary>
        /// Base game clears the starting possessions dict when it generates possessions. At that time this mod will already have generated possessions, so base game is redundant
        /// </summary>
        [HarmonyPrefix]
        public static bool PreventVanillaPossessions()
        {
            if(PfESettings.IsDevModeEnabled)
                Log.Message($"Preventing base game possession generation");
            return false;
        }
    }
}
