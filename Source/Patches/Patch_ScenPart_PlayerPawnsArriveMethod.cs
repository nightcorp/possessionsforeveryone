﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone
{
    [HarmonyPatch(typeof(ScenPart_PlayerPawnsArriveMethod), "GenerateIntoMap")]
    public static class Patch_ScenPart_PlayerPawnsArriveMethod
    {
        /// <summary>
        /// Remove base games starting possessions spawning, we are adding possessions during pawn generation to their inventory, so we never want on-map items
        /// </summary>
        [HarmonyPrefix]
        public static void RemoveVanillaStartingPossessions()
        {
            try
            {
                Dictionary<Pawn, List<ThingDefCount>> possessions = Find.GameInitData?.startingPossessions;
                if (possessions.NullOrEmpty())
                {
                    return;
                }
                foreach (List<ThingDefCount> things in possessions.Values)
                {
                    things.Clear();
                }
                if (PfESettings.IsDevModeEnabled)
                    Log.Message($"Cleared vanilla possessions");
            }
            catch (Exception ex)
            {
                Log.Error($"Exception caught: {ex}");
            }
        }
    }
}
