﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace PossessionsForEveryone
{
    [HarmonyPatch(typeof(PawnGenerator), "GenerateOrRedressPawnInternal")]
    public static class Patch_PawnGenerator
    {
        [HarmonyPostfix]
        public static void AddPossessions(Pawn __result, PawnGenerationRequest request)
        {
            try
            {
                if (!__result.RaceProps.Humanlike)
                {
                    return;
                }

                if (request.AllowedDevelopmentalStages.Newborn())
                {
                    return;
                }

                if (ShouldSkipPossessions(request))
                {
                    if (PfESettings.IsDevModeEnabled)
                        Log.Message($"Skipping possession generation");

                    // during initial generation the dictionary for staring possessions MUST contain the pawns key
                    if (request.Context == PawnGenerationContext.PlayerStarter)
                    {
                        Find.GameInitData.startingPossessions.Add(__result, new List<ThingDefCount>());
                    }
                    return;
                }

                PossessionUtility.GenerateAndGivePossessionsTo(__result, out List<ThingDefCount> generatedThings);

                if (request.Context != PawnGenerationContext.PlayerStarter)
                {
                    return;
                }

                // during game-start pawn generation inject the generated items into the starting possessions dictionary for the game to display them when you pick colonists
                Dictionary<Pawn, List<ThingDefCount>> startingPossessions = Find.GameInitData.startingPossessions;
                startingPossessions.SetOrAdd(__result, generatedThings);
                if (PfESettings.IsDevModeEnabled)
                    Log.Message($"Added {__result.LabelShort}s possessions to starting possessions");
            }
            catch (Exception ex)
            {
                Log.Error($"Caught exception: {ex}");
            }
        }

        private static bool ShouldSkipPossessions(PawnGenerationRequest request)
        {
            if(!CanHavePossessions(request))
            {
                return true;
            }
            if(IsColonist(request))
            {
                return !Rand.Chance(PfESettings.ChanceForColonists);
            }
            if(IsVisitor(request))
            {
                return !Rand.Chance(PfESettings.ChanceForVisitors);
            }
            return !Rand.Chance(PfESettings.ChanceForRaiders);
        }

        private static bool IsColonist(PawnGenerationRequest request)
        {
            if(request.Context == PawnGenerationContext.PlayerStarter)
            {
                return true;
            }
            if(request.Faction != null && Faction.OfPlayerSilentFail != null && request.Faction == Faction.OfPlayerSilentFail)
            {
                return true;
            }
            return false;
        }

        private static bool IsVisitor(PawnGenerationRequest request)
        {
            Faction playerFaction = Faction.OfPlayerSilentFail;
            if(playerFaction == null)
            {
                if(PfESettings.IsDevModeEnabled)
                    Log.Message($"Could not figure out pawns aggression to player faction - no player faction exists");
                return false;
            }
            return request.Faction.HostileTo(playerFaction);
        }

        private static bool CanHavePossessions(PawnGenerationRequest request)
        {
            if(request.KindDef == PawnKindDefOf.Beggar)
            {
                return false;
            }
            return true;
        }
    }
}
